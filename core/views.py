from django.shortcuts import render

from pandas_datareader import data as pdr
import yfinance as yf
from qiskit_finance import QiskitFinanceError
from qiskit_finance.data_providers import *
from qiskit.utils import QuantumInstance
from qiskit_algorithms.utils import algorithm_globals
from qiskit_algorithms import NumPyMinimumEigensolver, QAOA, SamplingVQE
from qiskit_algorithms.optimizers import COBYLA
from qiskit_aer.primitives import Sampler
from qiskit_optimization.algorithms import MinimumEigenOptimizer
from qiskit.result import QuasiDistribution
from qiskit_finance.applications.optimization import PortfolioOptimization

from core.service import SomaDataAPI

import datetime
import matplotlib.pyplot as plt , mpld3
from pandas.plotting import register_matplotlib_converters
from core.forms import myForm
import io
import urllib, base64
import seaborn as sns
import numpy as np 
import pandas as pd
import os
from django.conf import settings as django_settings

#convert graph into string buffer and then convert 64 bit code into image
def converter (fig):
    
    buf = io.BytesIO()
    fig.savefig(buf,format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri1 =  urllib.parse.quote(string)
    return uri1   

def index (request):
    context = {'index': index , 'form': myForm()}
    return render (request,'core/index.html', context)

def chart (request):
    from pandas_datareader import data as pdr
    
    assets = request.POST.getlist('assets')
    stock_list = assets
    num_assets = len(stock_list)
    #risk_factor = 0.7
    #penalty = num_assets
    
    start = request.POST.get('start')
    end = request.POST.get('end')
    budget = request.POST.get('budget')
    
    sdt=datetime.datetime.strptime(start, '%Y-%m-%d')
    edt=datetime.datetime.strptime(end, '%Y-%m-%d')
    ys = sdt.year
    ms = sdt.month
    ds = sdt.day
    
    ye = edt.year
    me = edt.month
    de = edt.day
    
    start = datetime.datetime(ys, ms, ds)
    end = datetime.datetime(ye, me, de)

    stocks_list = stock_list
    start_date = start
    end_date = end

    # Generate Stock Data Frame with Soma Data Provider API 
    data = SomaDataAPI(stocks_list = stock_list, start_date=start, end_date=end)
    data.run()
    # Top 5 rows of data 
    df = data.stock_data
    #convert from Integer to String for each column type
    df.columns = df.columns.map(str)
    # replace each column name to corresponding asset name 
    for i in range(len(df.axes[1])):
        colname = df.columns[i]
        new_colname = data._stocks[i]
        df.rename(columns={colname:new_colname}, inplace=True)
    #df.head()
    
    #Closing Price
    fig, ax = plt.subplots(figsize=(15, 8))
    ax.plot(df)
    plt.title('Close Price History')
    plt.xlabel('Date',fontsize =20)
    plt.ylabel('Price in USD',fontsize = 20)
    ax.legend(df.columns.values)
    
    uri1 = converter (fig)
    
    # Correlation Matrix
    corr = df.corr()
    fig4, ax = plt.subplots(figsize=(8, 8))
    sns.heatmap(corr, mask=np.zeros_like(corr, dtype=np.bool_), annot=True, square=True, ax=ax, xticklabels=stock_list, yticklabels=stock_list)
    plt.title("Correlation between Equities")
    
    sns_plot = sns.pairplot(df)
    sns_plot.figure.savefig(os.path.join(django_settings.BASE_DIR, 'core/static/image/corm.png')) 
   
    uri4 = converter (fig4)
    
    # Mean value of each assets
    fig2, ax = plt.subplots(figsize=(6, 3))
        
    mu = data.get_period_return_mean_vector()
        
    ax.bar(stock_list,mu)
    plt.title("Expected Return of Each Asset")
        
    uri2 =  converter(fig2)
        
    # Covariance Matrix
    sigma = data.get_period_return_covariance_matrix()
    fig3, ax = plt.subplots(figsize=(8, 8))
    sns.heatmap(sigma, mask=np.zeros_like(sigma, dtype=np.bool_), annot=True, square=True, ax=ax, xticklabels=stock_list, yticklabels=stock_list)
    plt.title("Covarience Between Equities")
        
    uri3 = converter(fig3)
       
    #utility function 
    #purchase best stock 
    def selection_to_picks(num_assets, selection):
        purchase = []
        for i in range(num_assets):
            if selection[i] == 1:
                purchase.append(stock_list[i])
        return purchase
    
    #result of porfolio optimization 
    def print_result(result):
        selection = result.x
        value = result.fval
        #print("Optimal: selection {}, value {:.4f}".format(selection, value))

        eigenstate = result.min_eigen_solver_result.eigenstate
        probabilities = (
            eigenstate.binary_probabilities()
            if isinstance(eigenstate, QuasiDistribution)
            else {k: np.abs(v) ** 2 for k, v in eigenstate.to_dict().items()}
        )
        fn_selection = selection_to_picks(num_assets, selection)
        probabilities = sorted(probabilities.items(), key=lambda x: x[1], reverse=True)
        
        states, values, probs = [], [], []
        
        for k, v in probabilities:
            x = np.array([int(i) for i in list(reversed(k))])
            value = portfolio.to_quadratic_program().objective.evaluate(x)
            states.append(''.join(str(i) for i in x))
            values.append(value)
            probs.append(v)
            
        return fn_selection, states,values,probs
    
    #QAOA Portfolio Optimization 
    risk_factor = 0.7
    budget = int(budget)
    penalty = num_assets

    portfolio = PortfolioOptimization(
        expected_returns=mu, covariances=sigma, risk_factor=risk_factor, budget=budget
    )
    qubitOp = portfolio.to_quadratic_program()
    
    algorithm_globals.random_seed = 1234
    cobyla = COBYLA()
    cobyla.set_options(maxiter=250)
    qaoa_mes = QAOA(sampler=Sampler(), optimizer=cobyla, reps=3)
    qaoa = MinimumEigenOptimizer(qaoa_mes)
    result = qaoa.solve(qubitOp)
    fn_selection,states,values,probs = print_result(result)
    
    # Generate Dataframe of Result of Possible Combination Assets
    data = {"State": states,
        "Value": values,
        "Probability": probs
        }
    # Create DataFrame
    df = pd.DataFrame(data)
    
    #Write df to HTML
    boostrap_link = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" >'
    script1 = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>'
    script2 = '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>'

    # render dataframe as html
    html = df.to_html()

    # replace default class with bootstrap classes
    html = html.replace('class="dataframe"', 'class="table table-striped"')

    # write html to file
    text_file=open(os.path.join(django_settings.BASE_DIR, 'core/templates/core/result.html'),'w')

    text_file.write(boostrap_link + '\n' + script1 + '\n'+script2 + '\n'+ html)
    text_file.close()
    
    # Ploting Probabilties of Possible Comibination Assets
    fig5, ax = plt.subplots(figsize=(20, 8))
    optimized_value = sns.barplot(x = "State", y = "Probability", data = df)
    for item in optimized_value.get_xticklabels():
        item.set_rotation(45)
    
    plt.title("Possible Combination of Assets", fontsize=15) 
    plt.xlabel('Possible Combinations of Assets',fontsize =10)
    plt.ylabel('Probability',fontsize = 20)
    #plt.show()
    uri5 =  converter(fig5)
    
    return render(request,'core/chart.html',{ 'data2':uri2 , 'data3':uri3 , 'data4':uri4,
                                              'data5': fn_selection, 'data6':uri5 ,'data1': uri1 })