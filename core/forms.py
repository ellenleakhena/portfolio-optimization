from django import forms

class myForm(forms.Form):
    OPTIONS = [
        ("META", "META"),
        ("AAPL", "AAPL"),
         ("AMZN", "AMZN"),
        ("NFLX", "NFLX"),
        ("GOOGL", "GOOGL"),
        ("MSFT", "MSFT"),
        ]

    assets= forms.MultipleChoiceField(
            choices=OPTIONS,
            initial='0',
            widget=forms.SelectMultiple(attrs={'class':'form-control'}),
            required=True,
            label='',
    )
    
    start = forms.DateField (widget=forms.DateInput(attrs={'type':'date', 'class':'form-control'}))
    end   = forms.DateField (widget=forms.DateInput(attrs={'type':'date','class':'form-control'}))
    budget = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))