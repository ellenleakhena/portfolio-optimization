from qiskit_finance.data_providers import *
from qiskit_finance import QiskitFinanceError
import pandas as pd
import datetime

class SomaDataAPI(BaseDataProvider):
    """
    The abstract base class for Soma data_provider.
    """
    def __init__(self, stocks_list, start_date, end_date):
        '''
        stocks -> List of interested assets
        start -> start date to fetch historical data
        end -> end date to fetch historical data
        '''
        super().__init__()
        self._stocks = stocks_list
        self._start = start_date
        self._end = end_date
        self._data = []
        self.stock_data = pd.DataFrame()

    def run(self):
        self._data = []
        stocks_notfound = []
        data = YahooDataProvider(self._stocks,
                  start=self._start,
                  end=self._end,
              )
        data.run()
        for (cnt, s) in enumerate(data._tickers):
            stock_value = data._data[cnt]
            self.stock_data[cnt] = data._data[cnt]
            if stock_value.dropna().empty:
                stocks_notfound.append(cnt)
            self._data.append(stock_value)